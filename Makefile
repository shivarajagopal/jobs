#==========================================================================
# Makefile for XeLaTeX build through latexmk
#==========================================================================

all: clean init
	@echo "Make all projects..."
	@for F in $$(cat projects) ; \
		do echo $$F; \
		cd $$F; \
		make once ;\
		cd ..; \
	done;

# Clean up the directory
clean:
	@echo "clean all dependencies for all projects..."
	@for F in $$(cat projects) ; \
		do echo $$F; \
		cd $$F; \
		if [ -e "Makefile" ] ;\
		then \
		  make clean ;\
		fi ;\
		for filename in $$(\ls ../deps) ; \
			do rm -rf $$filename; \
		done; \
		cd ..; \
	done;

clobber:
	@echo "clobber all dependencies for all projects..."
	@for F in $$(cat projects) ; \
		do echo $$F; \
		cd $$F; \
		if [ -e "Makefile" ] ;\
		then \
		  make clobber ;\
		fi ;\
		for filename in $$(\ls ../deps) ; \
			do rm -rf $$filename; \
		done; \
		cd ..; \
	done;

init: 
	@echo "initializing all projects for this system..."
	@for F in $$(cat projects) ; \
		do echo $$F; \
		cd $$F; \
		for filename in $$(\ls ../deps) ; \
			do ln -s ../deps/$$filename $$filename; \
		done; \
		cd ..; \
	done;

.PHONY: clean all init

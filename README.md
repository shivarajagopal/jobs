# Shiva Rajagopal Resume


This is my LaTeX template for a **CV(Curriculum Vitae)**, **Résumé** or **Cover Letter** inspired by [Awesome CV](https://github.com/posquit0/Awesome-CV/). It is easy to customize your own template, especially since it is really written by a clean, semantic markup.


## How to Use

#### Requirements

A full TeX distribution is assumed.  [Various distributions for different operating systems (Windows, Mac, \*nix) are available](http://tex.stackexchange.com/q/55437) but TeX Live is recommended.
You can [install TeX from upstream](http://tex.stackexchange.com/q/1092) (recommended; most up-to-date) or use `sudo apt-get install texlive-full` if you really want that.  (It's generally a few years behind.)

Additionally, you are expected to have GNU Make installed, as well as the extremely useful latexmk utility. This should all be included in a traditional install of TeXLive.

All documents use a central copy of their common requirements, including fonts and the Makefile, located in the ``deps`` folder. Do not touch these files unless you know what you're doing! All project folders contain symbolic links to the files in the ``deps`` folder. If you want to create a new document, you should first copy one of the template folders and only modify the LaTeX files, so that the build system is not affected.

Also, very importantly, **your main .tex file must have the same name as the folder it is in for the Makefile to work correctly!**

#### Usage

Before you do anything, you'll want to make sure the dependencies for each project are symlinked into the subprojects. Since symlinks get hairy from machine to machine, I decided to use a top-level Makefile (yes, I know there are better ways). Anyway, **before you do anything, run this from the top directory:**

```bash
$ make
```

From here on, we'll be working in the subdirectory you intend to build (e.g. resume). Within the subproject directory, there is another Makefile that you'll use to build the project.
At a command prompt, run:

```bash
$ make
```

This should result in the creation of ``{your-cv}.pdf``, and keep ``latexmk`` running so that all edits to the source files get updated. You should set up ``latexmk`` so that your file opens with a PDF reader that does not lock the file for editing (e.g. SumatraPDF or Preview).

You can also use 

```bash
$ make once
```

if you don't want latexmk constantly running. 

## Credit

[**LaTeX**](http://www.latex-project.org) is a fantastic typesetting program that a lot of people use these days, especially the math and computer science people in academia.

[**LaTeX FontAwesome**](https://github.com/furl/latex-fontawesome) is bindings for FontAwesome icons to be used in XeLaTeX.

[**Roboto**](https://github.com/google/roboto) is the default font on Android and ChromeOS, and the recommended font for Google’s visual language, Material Design.

[**Source Sans Pro**](https://github.com/adobe-fonts/source-sans-pro) is a set of OpenType fonts that have been designed to work well in user interface (UI) environments.


## Contact

You are free to take my `.tex` file and modify it to create your own resume. Please don't use my resume for anything else without my permission, or the permission of posquit0.

Good luck!
